﻿using System;

namespace Arithmetic_Operators_1._1
{
    internal class Program
    {
        static void Main(string[] args)
        {
            // Имеется 3 переменные типа int x = 10, y = 12, и z = 3;
            // Выполните и рассчитайте результат следующих операций для этих переменных:
            // x += y - x++ * z;
            // z = --x – y* 5;
            // y /= x + 5 % z;
            // z = x++ + y* 5;
            // x = y - x++ * z;
            // Сформулируйте вывод о порядке действий
        }


    class Program
        {
            static void Main(string[] args)
            {
                int x = 10, y = 12, z = 3;

                x += y - x++ * z;
                Console.WriteLine("x: " + x);
                z = --x - y * 5;
                Console.WriteLine("z: " + z);
                y /= x + 5 % z;
                Console.WriteLine("y: " + y);
                z = x++ + y * 5;
                Console.WriteLine("z: " + z);
                x = y - x++ * z;
                Console.WriteLine("x: " + x);

                // Вывод
                // Если операнд ++ (--) находится слева от переменной, то он выполняется в приоритете (=> входит в расчёт)
                Console.ReadKey();
            }
        }
    }

}


